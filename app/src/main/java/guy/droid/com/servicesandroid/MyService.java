package guy.droid.com.servicesandroid;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Created by RASHIK on 1/25/2016.
 */
public class MyService extends Service {
    public static final long NOTIFY_INTERVAL = 3 * 1000; // 10 seconds
    android.os.Handler handler = new android.os.Handler();
    java.util.Timer timer = null;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if(timer!=null)
        {
            timer.cancel();
        }
        else
        {
            timer = new java.util.Timer();
        }
        timer.scheduleAtFixedRate(new Timer(),0,NOTIFY_INTERVAL);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(getApplicationContext(),"SERVICE STARTED",Toast.LENGTH_SHORT).show();
        try{

      /*  handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(),"RUNNIG",Toast.LENGTH_SHORT).show();
            }
        },5000);*/
           /* handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(),"RUNNIG",Toast.LENGTH_SHORT).show();
                }
            });*/


        }catch (Exception e)
        {
            Log.w("RAZ","  "+e);
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(getApplicationContext(),"SERVICE STOPPPED",Toast.LENGTH_SHORT).show();
        timer.cancel();
    }
    class Timer extends TimerTask{

        @Override
        public void run() {
            handler.post(new Runnable() {
                @Override
                public void run() {
                   Toast.makeText(getApplicationContext(),"RUNNING",Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


}
